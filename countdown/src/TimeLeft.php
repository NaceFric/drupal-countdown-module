<?php

namespace Drupal\countdown;

class TimeLeft {

    public function daysUntilEvent($date)
    {
        $difference = $this->getDifferenceInDays($date);

        if($difference >= 1)
        {
            return "Days left to event start: " . $difference;
        }
        else if($difference == 0)
        {
            return "This event is happening today.";
        }
        else
        {
            return "The event has ended.";
        }
    }

    public function getDifferenceInDays($date)
    {
        $now = time();
        $event_date = strtotime($date);
        $difference = $event_date - $now;
        return round($difference / (60 * 60 * 24));
    }

}