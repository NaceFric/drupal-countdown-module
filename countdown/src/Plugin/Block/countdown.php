<?php

namespace Drupal\countdown\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a countdown Block.
 *
 * @Block(
 *   id = "countdown_block",
 *   admin_label = @Translation("countdown block"),
 * )
 */

class countdown extends BlockBase {

    public function build()
    {
        $node = \Drupal::routeMatch()->getParameter('node');
        $date = $node->get("field_event_date")->getValue();
        $date = $date[0]['value'];
        $output = \Drupal::service('countdown.time_left')->daysUntilEvent($date);

        return array(
            '#markup' => $output,
            '#cache' => [
                'max-age' => 0,
            ],
        );
    }
}